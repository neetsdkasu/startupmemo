VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   0  'なし
   Caption         =   "覚書メモ"
   ClientHeight    =   3495
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4815
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3495
   ScaleWidth      =   4815
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command1 
      Height          =   180
      Left            =   4440
      TabIndex        =   3
      Top             =   120
      Width           =   255
   End
   Begin VB.TextBox Text2 
      BorderStyle     =   0  'なし
      Height          =   270
      Left            =   240
      Locked          =   -1  'True
      TabIndex        =   2
      Text            =   "Text2"
      Top             =   360
      Width           =   4335
   End
   Begin VB.Timer Timer2 
      Interval        =   1000
      Left            =   720
      Top             =   3360
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   240
      Top             =   3360
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'ﾌﾗｯﾄ
      BorderStyle     =   0  'なし
      Height          =   2535
      Left            =   240
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  '垂直
      TabIndex        =   0
      Text            =   "Form1.frx":0000
      Top             =   720
      Width           =   4335
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   255
      Left            =   360
      TabIndex        =   1
      Top             =   0
      Width           =   2535
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFC0C0&
      BackStyle       =   1  '不透明
      Height          =   200
      Left            =   30
      Shape           =   3  '円
      Top             =   30
      Width           =   200
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000005&
      X1              =   0
      X2              =   4800
      Y1              =   1
      Y2              =   1
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Dim Flag1 As Integer
Dim mdX As Single, mdY As Single

Private Sub Command1_Click()
    If Timer1.Enabled Then
        Timer1.Enabled = False
    End If

    SetTopMostWindow Me.hwnd, False
    PopupMenu Form2.mnuMenu
    SetTopMostWindow Me.hwnd, True
    If FinishFlag Then
        Unload Form2
        Unload Me
    End If
End Sub

Private Sub Form_DblClick()
    If Flag1 = 1 Then
        Me.Move 0, 0, 240, 240
        Flag1 = 0
    Else
        Me.Width = Text1.Width + 480
        Me.Height = Label1.Height + Text2.Height + Text1.Height + 480
        Flag1 = 1
    End If
End Sub

Private Sub Form_Load()
    Dim n As Integer
    Dim s As String
    
    Flag1 = 1
    Timer1.Enabled = True
    FinishFlag = False
    s = Date & " (" & Mid$("日月火水木金土", Weekday(Date), 1) & ") " & Time
    Label1.Caption = s 'String$(6, " ") & s
    'Label1.ToolTipText = s
    
    Text1.Text = GetTextData()
    
    n = Weekday(Date)
    Text2.Text = GetWeekText(n)
    
    Form2.mnuDate.Caption = s
    
    SetTopMostWindow Me.hwnd, True
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Timer1.Enabled Then
        Timer1.Enabled = False
    End If
    
    Select Case Button
    Case vbLeftButton
        mdX = x
        mdY = y
    Case vbRightButton
        SetTopMostWindow Me.hwnd, False
        PopupMenu Form2.mnuMenu
        SetTopMostWindow Me.hwnd, True
        If FinishFlag Then
            Unload Form2
            Unload Me
        End If
    End Select
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbLeftButton Then
        Me.Left = Me.Left + (x - mdX)
        Me.Top = Me.Top + (y - mdY)
    End If
End Sub


Private Sub Text1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Timer1.Enabled Then
        Timer1.Enabled = False
    End If
End Sub

Private Sub Timer1_Timer()
    Static iCount As Integer
    
    iCount = iCount + 1
    
    If iCount > (OpenTime - 1) Then
        Me.Move 0, 0, 240, 240
        Flag1 = 0
        iCount = 0
        Timer1.Enabled = False
        If AutoEndFlag Then
            Unload Form2
            Unload Me
        End If
    End If
    
End Sub

Private Sub Timer2_Timer()
    Dim s As String
    s = Date & " (" & Mid$("日月火水木金土", Weekday(Date), 1) & ") " & Time
    Label1.Caption = s 'String$(6, " ") & s
    'Label1.ToolTipText = s
    Form2.mnuDate.Caption = s
End Sub
