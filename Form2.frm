VERSION 5.00
Begin VB.Form Form2 
   BorderStyle     =   3  '固定ﾀﾞｲｱﾛｸﾞ
   Caption         =   "設定"
   ClientHeight    =   4185
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4830
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4185
   ScaleWidth      =   4830
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  '画面の中央
   Begin VB.TextBox Text3 
      Height          =   270
      Left            =   960
      MaxLength       =   100
      TabIndex        =   9
      Text            =   "Text3"
      Top             =   120
      Width           =   3615
   End
   Begin VB.ComboBox Combo1 
      Height          =   300
      ItemData        =   "Form2.frx":0000
      Left            =   240
      List            =   "Form2.frx":0019
      Style           =   2  'ﾄﾞﾛｯﾌﾟﾀﾞｳﾝ ﾘｽﾄ
      TabIndex        =   8
      Top             =   120
      Width           =   615
   End
   Begin VB.TextBox Text2 
      Height          =   270
      IMEMode         =   3  'ｵﾌ固定
      Left            =   1080
      MaxLength       =   3
      TabIndex        =   5
      Text            =   "0"
      Top             =   3720
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Caption         =   "OK"
      Height          =   375
      Left            =   3600
      TabIndex        =   4
      Top             =   3720
      Width           =   975
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2520
      TabIndex        =   3
      Top             =   3720
      Width           =   975
   End
   Begin VB.CheckBox Check2 
      Caption         =   "メモ表示後自動終了"
      Enabled         =   0   'False
      Height          =   255
      Left            =   2520
      TabIndex        =   2
      Top             =   3360
      Width           =   2055
   End
   Begin VB.CheckBox Check1 
      Caption         =   "スタートアップ表示する"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   3360
      Width           =   2055
   End
   Begin VB.TextBox Text1 
      Height          =   2775
      Left            =   240
      MultiLine       =   -1  'True
      ScrollBars      =   2  '垂直
      TabIndex        =   0
      Text            =   "Form2.frx":0039
      Top             =   480
      Width           =   4335
   End
   Begin VB.Label Label2 
      Caption         =   "秒"
      Height          =   255
      Left            =   1680
      TabIndex        =   7
      Top             =   3765
      Width           =   255
   End
   Begin VB.Label Label1 
      Caption         =   "表示時間"
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   3765
      Width           =   735
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "メニュー"
      Visible         =   0   'False
      Begin VB.Menu mnuDate 
         Caption         =   "Date"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEdit 
         Caption         =   "設定(&E)"
      End
      Begin VB.Menu mnuBar 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "終了(&X)"
      End
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim weektext(6) As String

Private Sub Check1_Click()
    If Check1.Value = vbChecked Then
        Check2.Enabled = True
    Else
        Check2.Enabled = False
    End If
    
End Sub

Private Sub Combo1_Click()
    Text3.Text = weektext(Combo1.ListIndex)
End Sub

Private Sub Command1_Click()
    Dim i As Integer
    
    If Check1.Value = vbChecked Then
        SetStartupFlag True
        
        If Check2.Value = vbChecked Then
            SetAutoEndFlag True
        Else
            SetAutoEndFlag False
        End If
        
    Else
        SetStartupFlag False
    End If
    
    SetTextData Text1.Text
    SetOpenTime CInt(Text2.Text)
    
    For i = 0 To 6
        SetWeekText i + 1, weektext(i)
    Next i
    
    Form1.Text1.Text = Text1.Text
    
    i = Weekday(Date) - 1
    Form1.Text2.Text = weektext(i)
    
    Unload Me
End Sub

Private Sub Command2_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Dim i As Integer
    
    If GetStartupFlag() Then
        Check1.Value = vbChecked
        Check2.Enabled = True
    Else
        Check1.Value = vbUnchecked
        Check2.Enabled = False
    End If
    
    If GetAutoEndFlag() Then
        Check2.Value = vbChecked
    Else
        Check2.Value = vbUnchecked
    End If
    
    Text1.Text = Form1.Text1.Text
    Text2.Text = CStr(GetOpenTime())
    
    For i = 0 To 6
        weektext(i) = GetWeekText(i + 1)
    Next i
    
    Combo1.ListIndex = Weekday(Date) - 1
    
End Sub

Private Sub mnuEdit_Click()
    Me.Show vbModal, Form1
End Sub

Private Sub mnuExit_Click()
    FinishFlag = True
End Sub

Private Sub Text2_Change()
    Dim n As Integer
    
    n = Text2.SelStart
    If n > 0 Then
        If InStr(1, "1234567890", Mid$(Text2.Text, n, 1)) < 1 Then
            Text2.SelStart = n - 1
            Text2.SelLength = 1
            Text2.SelText = ""
        End If
    End If
End Sub

Private Sub Text2_LostFocus()
    Dim n As Integer
    
    n = CInt(Text2.Text)
    If n < 0 Then
        n = 0
    End If
    Text2.Text = CStr(n)
    
End Sub

Private Sub Text3_LostFocus()
    weektext(Combo1.ListIndex) = Text3.Text
End Sub
