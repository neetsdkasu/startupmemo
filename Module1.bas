Attribute VB_Name = "Module1"
Option Explicit


Private Declare Function GetPrivateProfileString _
    Lib "kernel32" Alias "GetPrivateProfileStringA" ( _
        ByVal lpApplicationName As String, _
        ByVal lpKeyName As Any, _
        ByVal lpDefault As String, _
        ByVal lpReturnedString As String, _
        ByVal nSize As Long, _
        ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString _
    Lib "kernel32" Alias "WritePrivateProfileStringA" ( _
        ByVal lpApplicationName As String, _
        ByVal lpKeyName As Any, _
        ByVal lpString As Any, _
        ByVal lpFileName As String) As Long


'ウィンドウを常に手前表示する

Public Const SWP_NOMOVE = 2
Public Const SWP_NOSIZE = 1
Public Const FLAGS = SWP_NOMOVE Or SWP_NOSIZE
Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2

Declare Function SetWindowPos Lib "user32" _
      (ByVal hwnd As Long, _
      ByVal hWndInsertAfter As Long, _
      ByVal x As Long, _
      ByVal y As Long, _
      ByVal cx As Long, _
      ByVal cy As Long, _
      ByVal wFlags As Long) As Long



Private Const STRING_LENGTH As Integer = 100
Private Const PPFileTitle As String = "setting.ini"
Private Const TxFileTitle As String = "text.dat"
Private Const APP_NAME As String = "PROGRAM026"
Private Const KEY_STUP As String = "StartUp"
Private Const KEY_ATED As String = "AutoEnd"
Private Const KEY_OPTM As String = "OpenTime"
Private PPFilePath As String
Private TxFilePath As String
Public FinishFlag As Boolean
Public AutoEndFlag As Boolean
Public OpenTime As Integer

Public Function SetTopMostWindow(hwnd As Long, Topmost As Boolean) _
   As Long

   If Topmost = True Then 'Make the window topmost
      SetTopMostWindow = SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, _
         0, FLAGS)
   Else
      SetTopMostWindow = SetWindowPos(hwnd, HWND_NOTOPMOST, 0, 0, _
         0, 0, FLAGS)
      SetTopMostWindow = False
   End If
End Function


Private Function Trim0(str1 As String) As String
    Dim n As Integer
    n = InStr(1, str1, vbNullChar)
    If n > 1 Then
        Trim0 = Left$(str1, n - 1)
    Else
        Trim0 = ""
    End If
        
End Function

Private Sub Init_SaveFile()
    Dim p As String
    
    p = App.Path
    If Right$(p, 1) <> "\" Then
        p = p & "\"
    End If
    
    PPFilePath = p & PPFileTitle
    TxFilePath = p & TxFileTitle
    
End Sub

Private Function GPPS(strAPP As String, strKEY As String, strDft As String, strDat As String) As Long

    Dim s As String * STRING_LENGTH

    GPPS = GetPrivateProfileString(strAPP, strKEY, strDft, s, STRING_LENGTH, PPFilePath)
    
    strDat = Trim0(s)

End Function

Private Function WPPS(strAPP As String, strKEY As String, strDat As String) As Long
    
    WPPS = WritePrivateProfileString(strAPP, strKEY, strDat, PPFilePath)
    
End Function

Public Function GetStartupFlag() As Boolean
    Dim s As String
    
    GPPS APP_NAME, KEY_STUP, CStr(True), s
    
    GetStartupFlag = CBool(s)
        
End Function

Public Sub SetStartupFlag(bool1 As Boolean)
    Dim s As String
    
    s = CStr(bool1)
    WPPS APP_NAME, KEY_STUP, s
End Sub

Public Function GetAutoEndFlag() As Boolean
    Dim s As String
    
    GPPS APP_NAME, KEY_ATED, CStr(False), s
    
    GetAutoEndFlag = CBool(s)
        
End Function

Public Sub SetAutoEndFlag(bool1 As Boolean)
    Dim s As String
    
    s = CStr(bool1)
    WPPS APP_NAME, KEY_ATED, s
End Sub

Public Function GetOpenTime() As Integer
    Dim s As String
    
    GPPS APP_NAME, KEY_OPTM, "3", s
    
    GetOpenTime = CInt(s)
        
End Function

Public Sub SetOpenTime(intTime1 As Integer)
    Dim s As String
    
    s = CStr(intTime1)
    WPPS APP_NAME, KEY_OPTM, s
End Sub

Public Function getWeekKey(wk As Integer) As String
    If wk < 1 Then
        getWeekKey = "日"
    ElseIf wk > 7 Then
        getWeekKey = "日"
    Else
        getWeekKey = Mid$("日月火水木金土", wk, 1)
    End If
End Function

Public Function GetWeekText(wk As Integer) As String
    Dim key1 As String, str1 As String
    
    key1 = getWeekKey(wk)
    GPPS APP_NAME, key1, "", str1
    
    GetWeekText = str1
End Function

Public Sub SetWeekText(wk As Integer, str1 As String)
    Dim key1 As String
    
    key1 = getWeekKey(wk)
    WPPS APP_NAME, key1, str1
End Sub

Public Function GetTextData() As String
    Dim fso1 As New FileSystemObject
    Dim tsm1 As TextStream
        
    If fso1.FileExists(TxFilePath) Then
               
        Set tsm1 = fso1.OpenTextFile(TxFilePath)
        
        If tsm1.AtEndOfStream Then
            GetTextData = ""
        Else
            GetTextData = tsm1.ReadAll
        End If
        
        tsm1.Close
        
        Set tsm1 = Nothing
        
    Else
        
        GetTextData = ""
        
    End If
    
End Function

Public Sub SetTextData(str1 As String)
    Dim fso1 As New FileSystemObject
    Dim tsm1 As TextStream
        
    Set tsm1 = fso1.CreateTextFile(TxFilePath)
    
    tsm1.Write str1
    
    tsm1.Close
    
    Set tsm1 = Nothing
        
End Sub

Sub Main()
    Dim s As String
    
    s = Trim$(Command$())
    
    Init_SaveFile
    
    If s = "-s" Then
        If GetStartupFlag() Then
            OpenTime = GetOpenTime()
            AutoEndFlag = GetAutoEndFlag()
            Form1.Show
        End If
    Else
        OpenTime = 600
        AutoEndFlag = False
        Form1.Show
    End If
    
End Sub
